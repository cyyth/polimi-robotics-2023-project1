#include "ros/ros.h"

// Messages and utils
#include "tf/transform_broadcaster.h"
#include "geometry_msgs/Quaternion.h"
#include "geometry_msgs/Twist.h"
#include "geometry_msgs/Pose.h"
#include "geometry_msgs/TransformStamped.h"
#include "nav_msgs/Odometry.h"
#include "std_msgs/Header.h"

// Custom Messages
#include "first_project/Odom.h"

// Custom Services
#include "first_project/ResetOdom.h"

// std libraries
#include <math.h>

class OdometryPublisher {
public:
	OdometryPublisher() {
		// Retrieving parameters
		n_.getParam("/easymile_ez10_wheelbase", c_wheelbase);
		n_.getParam("/starting_x", x0_);
		n_.getParam("/starting_y", y0_);
		n_.getParam("/starting_th", theta0_);
		c_frame_id = "odom";
		c_child_frame_id = "base_link";

		// Set up the reset service
    	reset_service_ = n_.advertiseService("reset_odom", &OdometryPublisher::_reset, this);
		ROS_INFO("Odometry reset service is ready.");

		// Set up the odometry publishers
		odom_publisher_ = n_.advertise<nav_msgs::Odometry>("/odometry", 1000);
		custom_odom_publisher_ = n_.advertise<first_project::Odom>("/custom_odometry", 1000);

		// Subscribe to the topic
		steer_subscriber_ = n_.subscribe<geometry_msgs::Quaternion>("/speed_steer", 1000,
																	&OdometryPublisher::_odomCallback, this);
	}


private:
	ros::NodeHandle n_;
	ros::ServiceServer reset_service_;
	ros::Publisher odom_publisher_;
	ros::Publisher custom_odom_publisher_;
	ros::Subscriber steer_subscriber_;
	tf::TransformBroadcaster odom_broadcaster_;
	ros::Time current_time_, last_time_;

	double x_, y_, theta_;
	double x0_, y0_, theta0_;
	bool init_ = false;

	double c_wheelbase;
	std::string c_frame_id;
	std::string c_child_frame_id;

	bool _reset(first_project::ResetOdom::Request& req,first_project::ResetOdom::Response& res) {
		/* This only reset the pose, robot keeps its velocity. */
		x_ = 0.0;
		y_ = 0.0;
		theta_ = 0.0;
		res.resetted = true;
		ROS_WARN("THE ODOMETRY HAS BEEN RESET.");
		return true;
	}

	void _odomCallback(const geometry_msgs::Quaternion::ConstPtr& msg) {
		nav_msgs::Odometry odom_msg;
		first_project::Odom custom_odom_msg;
		geometry_msgs::TransformStamped tf_msg;
		double dtheta, dt;

		if (!init_) {
			/* Since dt is not known yet, we can not compute theta. If either dtheta (e.g. 0.4 rad/s)
			or dt is given (e.g. 1/50 s), we can use that instead of dtheta = 0.0. Another way is to
			assume the robot is at rest when the program starts, i.e. V_x = 0, dtheta = 0.*/

			current_time_ = ros::Time::now();
			last_time_ = current_time_;
			x_ = x0_;
			y_ = y0_;
			theta_ = theta0_;
			dtheta = 0.0;
			init_ = true;
		} else {
			current_time_ = ros::Time::now();

			// Euler integration of the Ackermann steering kinematics
			dt = (current_time_-last_time_).toSec();
			dtheta = (msg->x/c_wheelbase)*tan(msg->y)*dt;
			theta_ += dtheta;
			x_ += msg->x*cos(theta_)*dt;
			y_ += msg->x*sin(theta_)*dt;

			last_time_ = current_time_;
		}

		/*
		Broadcasting the geometry_msgs/TransformStamped
		*/
		// Assign the header
		tf_msg.header.frame_id = c_frame_id;
		tf_msg.child_frame_id = c_child_frame_id;
		tf_msg.header.stamp = current_time_;

		// Assigning the pose
		tf_msg.transform.translation.x = x_;
		tf_msg.transform.translation.y = y_;
		tf_msg.transform.translation.z = 0.0;
		tf_msg.transform.rotation = tf::createQuaternionMsgFromYaw(theta_);

		// Broadcast the transformation
        odom_broadcaster_.sendTransform(tf_msg);

		/*
		Standard nav_msgs/Odometry
		*/
		// Assign the header (copy it from the tf)
		odom_msg.header = tf_msg.header;

		// Assigning the TWIST part
		odom_msg.twist.twist.linear.x = msg->x;
		odom_msg.twist.twist.linear.y = 0.0;
		odom_msg.twist.twist.linear.z = 0.0;

		odom_msg.twist.twist.angular.x = 0.0;
		odom_msg.twist.twist.angular.y = 0.0;
		odom_msg.twist.twist.angular.z = dtheta;

		// Assigning the POSE part
		odom_msg.pose.pose.position.x = x_;
		odom_msg.pose.pose.position.y = y_;
		odom_msg.pose.pose.position.z = 0.0;
		odom_msg.pose.pose.orientation = tf_msg.transform.rotation;

		// Publish the nav odometry
		odom_publisher_.publish(odom_msg);

		/*
		Customized odometry message (first_project/Odom)
		*/
		custom_odom_msg.x = x_;
		custom_odom_msg.y = y_;
		custom_odom_msg.th = theta_;
		custom_odom_msg.timestamp = std::__cxx11::to_string(current_time_.toSec());

		// Publish the custom odometry
		custom_odom_publisher_.publish(custom_odom_msg);

		// set node output to "screen" to show this
		ROS_INFO("\nSpeed: %.2f [m/s]\nSteering Angle: %.1f [degree]\n"
				 "---\nX Position: %.2f [m]\nY Position: %.2f [m]\nYaw: %.1f [degree]\n",
				 msg->x, msg->y*180.0/3.1416, x_, y_, theta_*180/3.1416);
	}

};

int main(int argc, char** argv) {
	// Initialize the node
    ros::init(argc, argv, "odom_node");

	OdometryPublisher op;

	ros::spin();

	return 1;
}
