### The first_project package of the Politecnico di Milano Robotics Course 2022/2023 (Prof. Matteucci and Mentasti)

- The group has one member.

- For custom odometry message, timestamp part, I'm not sure what to put in, so I just assign the string of the current simulation time in second.

- The launch file is named <first_project.launch>, which automatically runs first or second bag file depends on the argument bag_name:=<FILE_NAME>. The bag file(s) should be added into the <path-to-workspace>/src/first_project/bags/ beforehand or the rosbag portion must be removed prior to launching.

- The package also included a rudimentary 3d model (urdf/easymile_ez10.urdf) of the EZ10 and the tf tree is structured as: /odom → /base_link → /chassis → /sick_<position> and /wheel_<position>
				
- /base_link is attached to the rear axle of the robot because it makes kinematic sense. And other components can be easier defined from the /chassis frame.

- Rosbag files are excluded from the repository because it is quite large.
